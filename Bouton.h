/*
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Bouton.h
 *
 *  Created on: Feb 2017
 *      Author: Vincent Limorté
 */

#ifndef _BOUTON_H_
#define _BOUTON_H_
#include "Arduino.h"

#define BOUTON_VERSION "v0.0.1"

typedef enum BMode {
	// LOW sur contact, HIGH au repos
	UP,
	// HIGH sur contact, LOW au repos
	DOWN,
	// LOW sur contact, HIGH au repos, pullup par arduino.
	PULLUP} BoutonMode;

class Bouton {
public:
	/**
	 *
	 */
	Bouton(uint8_t pin, BoutonMode mode, unsigned long debounceDelay = 50);
	void init();
	virtual ~Bouton();
	bool readDebounceButton();

private:
	int _pin;
	BoutonMode _mode;

	int _pushedState;

	int currentState ;          // the current reading from the input pin
	int lastState;   // the previous reading from the input pin

	// the following variables are long's because the time, measured in miliseconds,
	// will quickly become a bigger number than can be stored in an int.
	unsigned long lastDebounceTime;  // the last time the output pin was toggled
	unsigned long debounceDelay; // the debounce time; increase if the output flickers



};

#endif /* _BOUTON_H_ */
