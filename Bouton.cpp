/*
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * Bouton.cpp
 *
 *  Created on: Feb 2017
 *      Author: Vincent Limorté
 */

#include <Bouton.h>

Bouton::Bouton(uint8_t pin, BoutonMode mode, unsigned long debounceDelay) {
	_pin = pin;
	_mode = mode;
	lastDebounceTime = 0;
	this->debounceDelay=debounceDelay;
	if(_mode==DOWN){
		_pushedState = HIGH;
		currentState=LOW;
	} else {
		_pushedState = LOW;
		currentState=HIGH;
	}
	lastState=currentState;
	this->init();
}

void Bouton::init(){
	switch(_mode){
	case PULLUP:
		pinMode(_pin,INPUT_PULLUP);
		break;
	case UP:
	case DOWN:
		pinMode(_pin, INPUT);
		break;
	}

}

Bouton::~Bouton() {
	// TODO Auto-generated destructor stub
}

bool Bouton::readDebounceButton() {

	int reading = digitalRead(_pin);

	// check to see if you just pressed the button
	// (i.e. the input went from XXXX to _pushedState),  and you've waited
	// long enough since the last press to ignore any noise:

	// If the switch changed, due to noise or pressing:
	if (reading != lastState) {
		// reset the debouncing timer
		lastDebounceTime = millis();
	}

	if ((millis() - lastDebounceTime) > debounceDelay) {
		// whatever the reading is at, it's been there for longer
		// than the debounce delay, so take it as the actual current state:

		// if the button state has changed:
		if (reading != currentState) {
			currentState = reading;

			// only trigger if the new button state is LOW
			if (currentState == _pushedState) {
				//debugln("Pressé");
				lastState = reading;
				return true;
			}
		}
	}
	lastState = reading;
	return false;
}
