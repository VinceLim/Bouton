CC=avr-gcc
CPP=avr-c++
AR=avr-ar
SIZE=avr-size

MCU=atmega328p
F_CPU=16000000L
ARD_VER=105

LIB_DIRS=/usr/share/arduino/hardware/arduino/cores/arduino /usr/share/arduino/hardware/arduino/variants/eightanaloginputs
INC_LIBS=$(LIB_DIRS:%=-I%)

CC_FLAGS= -mmcu=$(MCU) -DF_CPU=$(F_CPU) -DARDUINO=$(ARD_VER)  -D__PROG_TYPES_COMPAT__ -Os -ffunction-sections -fdata-sections \
	$(INC_LIBS)
LD_FLAGS=-mmcu=$(MCU) -ffunction-sections -fdata-sections -Wl,--gc-sections -Os

SRC_CPP=$(wildcard *.cpp)
SRC_C=$(wildcard *.c)

libBouton.a: Bouton.o
	$(AR) rcs $@ Bouton.o 

Bouton.o: Bouton.cpp Bouton.h
	$(CPP) -c $(CC_FLAGS) -I. -o $@ Bouton.cpp
	
clean:
	rm -f *.o
	rm -f *.a

